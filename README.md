# Getting Started with Orbit Framework

If you're interested in Orbit, this is the place to start. 

### What is the Orbit Framework?

Orbit is a framework that can be used to assist in or facilitate the development of client-server applications using the Vue3 technology stack on the front-end and Python on the back-end. The connection between the front-end and back-end is made via websockets and secured by a public key encryption mechanism that rotates keys for every connection.

### Why is Orbit Framework?

The original concept revolves around transparently mapping data directly from a database to a rendered display in a browser. The idea came from looking at what "GraphQL" tries to achieve by keeping track of active queries and potentially refreshing them when a change is detected that might effect the query result. 

Ultimately we found the following;

* A query refresh resulting from a change could easily be 'expensive'
* A query refresh tends to involve reading / transmitting all data relating to the result set
* A fair bit of 'leg-work' seems to be involved in setting up and managing queries
* Queries are always subject to the performance and feature-set of the chosen Database

That's not to say there aren't ways around some (or all) of these issues, but at the time it seemed that from the perspective of our desired solution, there were fundamental issues to this approach.

On order to address these issues we looked at the following alternatives;

* A database that allowed for 'cheap' queries that didn't necessarily involve the reading and returning of entire records.
* The ability to treat the collection of results (i.e. result primary keys) and result data separately, so in the event of a query change, although a new primary key set would always be needed, only actual changed data need be transmitted. So for example the first page of a contact list of 20 entries. The page display returns a list of 20 records, however updating an entry results in the retransmission of only the contact with changed details.
* A move away from traditional SQL which effectively involves the database schema being defined and implemented in different places. (anti-DRY) We have adopted an approach where the database is effectively schema-less and the schema is implemented within the ORM itself. So there is no DDL to worry about, everything is on Python, and is only implemented once.
* Rather than rely on an external database, we're using an 'integral' database that makes up a part of the product. Whereas Orbit applications can easily interact with other databases, the database used to drive displays is a part of Orbit.

What he have as a result, heavily simplified, looks like this.

<img style="background-color: white;margin: 1em;border-radius:4px" src="images/orbit-1.svg" />

### What's in the box?

The framework includes or manages a number of features including;

* Creating template applications and components (via orbit-cli)
* Managing front/back end connections between the applications (and components)
* Managing the reactivity between the database and rendered screen components
* Packaging, "vite" templates on the front-end and "poetry" on the back-end
* Single file compilation using "pyinstaller"
* Packaging for Linux/Debian (Ubuntu), orbit-cli can generate a .deb file
* Multiple architectures, Orbit runs on multiple Linux based architectures and runs on a Mac
* The framework is UI toolkit agnostic
* The back-end can also service http(s) requests if necessary
* SSL self-signed key management is included for easy CloudFlare presentation
* no-SSL is available for use with CloudFlare ZeroTrust tunnels
* Orbit Components are completely modular, when written properly "any" Orbit Component can be incorporated into "any" Orbit Application via Orbit-CLI, so if someone has already written a conmponent you wish to use, it's literally a 1-line command.

### Sounds too good to be True, there must be a catch?

Well, of course! You're still responsible for the business logic and coding of any module you want to use that doesn't already exist. 

### What does it look like?

The "main" part of the code on Vue is managed for you, so "main.js" and "App.vue" are auto-generated from templates. The overall "look" of the application is governed by the module called "orbit-component-shell" which currently uses NAIVE UI by default. You can use this go get going or re-implement your own version of the shell component to provide whatever look you want using whatever UI framework you like. Take a look at the Orbit-CLI docs for more details, but to get going you can create an application with;

```bash
orbit-cli --init application
orbit-cli --add (application name) --components (components)
```

This will create a source tree with both "client" and "server" folders, and all the tooling required from running in development mode to building the .deb package.

The main Python code is also auto-generated and looks something like this;
```python
#!/usr/bin/env python
from orbit_component_base import OrbitMainBase

if __name__ == '__main__':
    OrbitMainBase('orbit-zerodocs').run()
```
This isn't something you should ever have to edit, the component framework adds and removes components dynamically so once installed, a component will start up as a part of the framework. If you don't want the component, remove it. (via poetry)

